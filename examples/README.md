This directory contains a number of examples for running BMPGA for various usecases.

Each example is self-contained in it's own directory will all the required input files etc.
To run the example, simply move into the directory and type: python Run.py

Note: Most of the implemented potentials rely on external programs (e.g. VASP, AMBER, NWChem etc.) and these will need to be installed on the system before the corresponding tests will run.  