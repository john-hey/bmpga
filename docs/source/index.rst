.. bmpga documentation master file, created by
   sphinx-quickstart on Sun Jun 10 16:44:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

bmpga: Birmingham Molecular Pool Genetic Algorithm
==================================================

A Genetic Algorithm (GA) for the global optimisation of gas-phase molecular and atomic clusters.

Source code: (CURRENTLY PRIVATE) https://bitbucket.org/john-hey/bmpga/

Documentation: (NOT ONLINE YET)


bmpga is a genetic algorithm and a package of associated tools for the global optimisation and energy landscape exploration of molecular and atomic clusters directly at the DFT level.
The main two main features of this package are: :ref:`GeneticAlgorithm <global_optimization>` this is used to search for the global minimum of a system and building a database of minima.
And :ref:`Threasholding <threashold_algorithm>` the implimentation of the threashold algorithm which can be used to extract information about the wider landscape.

Tutorials
---------

.. toctree::
   :maxdepth: 3

      setting_up_jobs
      defining_custom_potentials

Reference
---------

.. toctree::
   :maxdepth: 2

      global_optimization

..
   Contents:

   .. toctree::
      :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

